package com.example.schooltimetabling.models;

import java.time.DayOfWeek;

public class TimeslotsData {
    private DayOfWeek dayOfWeek;
    private String startTime;
    private String endTime;

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }
}
