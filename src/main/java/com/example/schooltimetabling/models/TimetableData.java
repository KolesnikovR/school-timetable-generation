package com.example.schooltimetabling.models;

import com.example.schooltimetabling.domain.Lesson;

public class TimetableData {
    public Lesson[] lessons;
    public TimeslotsData[] timeslots;
}
