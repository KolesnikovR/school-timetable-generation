package com.example.schooltimetabling.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.DayOfWeek;
import java.time.LocalTime;

import com.example.schooltimetabling.domain.Lesson;
import com.example.schooltimetabling.domain.TimeTable;
import com.example.schooltimetabling.domain.Timeslot;
import com.example.schooltimetabling.models.TimeslotsData;

@Service
@Transactional
public class TimeTableRepository {
    public static final Long SINGLETON_TIME_TABLE_ID = 1L;

    @Autowired
    private TimeslotRepository timeslotRepository;
    @Autowired
    private LessonRepository lessonRepository;

    public TimeTable findById(Long id) {
        if (!SINGLETON_TIME_TABLE_ID.equals(id)) {
            throw new IllegalStateException("There is no timeTable with id (" + id + ").");
        }
        return new TimeTable(
                timeslotRepository.findAll(),
                lessonRepository.findAll());
    }

    public void save(TimeTable timeTable) {
        for (Lesson lesson : timeTable.getLessonList()) {
            lessonRepository.save(lesson);
        }
    }

    public void saveLessons(Lesson[] lessons) {
        for (Lesson lesson : lessons) {
            lessonRepository.save(lesson);
        }
    }

    public void saveTimeslots(TimeslotsData[] timeslots) {
        for (TimeslotsData tData : timeslots) {
            DayOfWeek day = tData.getDayOfWeek();
            String[] startTimes = tData.getStartTime().split(":");
            String[] endTimes = tData.getEndTime().split(":");
            timeslotRepository.save(new Timeslot(
                day,
                LocalTime.of(Integer.parseInt(startTimes[0]), Integer.parseInt(startTimes[1])),
                LocalTime.of(Integer.parseInt(endTimes[0]), Integer.parseInt(endTimes[1]))
            ));
        }
    }
}
