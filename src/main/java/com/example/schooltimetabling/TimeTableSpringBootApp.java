

package com.example.schooltimetabling;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.example.schooltimetabling.persistence.LessonRepository;
import com.example.schooltimetabling.persistence.TimeslotRepository;

@SpringBootApplication
public class TimeTableSpringBootApp {

    public static void main(String[] args) {
        SpringApplication.run(TimeTableSpringBootApp.class, args);
    }

    @Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedMethods("*");
			}
		};
	}

    @Bean
    public CommandLineRunner demoData(
            TimeslotRepository timeslotRepository,
            LessonRepository lessonRepository) {
        return (args) -> {
            // Lesson lesson = lessonRepository.findAll(Sort.by("id")).iterator().next();
            // lesson.setTimeslot(timeslotRepository.findAll(Sort.by("id")).iterator().next());

            // lessonRepository.save(lesson);
        };
    }
}
